package main

import "fmt"

func main() {

	// https://adventofcode.com/2020/day/17
	// Initial universe: (0,0) is upper left
	// .#.
	// ..#
	// ###

	u := NewUniverse()
	u.AddNewCube(1, 0, 0).SetActive()
	u.AddNewCube(2, 1, 0).SetActive()
	u.AddNewCube(0, 2, 0).SetActive()
	u.AddNewCube(1, 2, 0).SetActive()
	u.AddNewCube(2, 2, 0).SetActive()

	maxCycle := 6
	for i := 1; i <= maxCycle; i++ {
		u = u.Cycle()
		fmt.Println(u.String())
		fmt.Printf("After cycle #%d there are %d active cubes\n", i, u.CountActiveCubes())
	}

}
