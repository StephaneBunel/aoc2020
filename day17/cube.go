package main

// Cube holds cube properties
type Cube struct {
	// Because a cube can only be active or inactive
	// a boolean is enought.
	active bool
}

// NewCube creates and returns a new Cube
func NewCube() *Cube {
	return new(Cube)
}

// SetActive makes the cube Active
func (c *Cube) SetActive() {
	c.active = true
}

// SetInactive make the cubre Inactive
func (c *Cube) SetInactive() {
	c.active = false
}

// IsActive returns true is the cube is active
func (c *Cube) IsActive() bool {
	return c.active
}
