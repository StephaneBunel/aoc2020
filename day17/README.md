One solution for [Day 17](https://adventofcode.com/2020/day/17) using  Golang.

How to run:

```sh
go run *.go
```
