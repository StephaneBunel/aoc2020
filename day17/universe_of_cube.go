package main

import "fmt"

// Universe holds the properties of a 3 dimensional space
type Universe struct {
	// universe is internaly a map of CubeCoordiante to Cube
	universe map[UniverseCoordinate]*Cube

	// Range of each 3 dimensions of the universe
	minX, maxX int
	minY, maxY int
	minZ, maxZ int
}

// UniverseCoordinate holds 3D coordinates in the universe
type UniverseCoordinate struct {
	x, y, z int
}

// NewUniverse returns a newly created Universe
func NewUniverse() *Universe {
	u := new(Universe)
	u.universe = make(map[UniverseCoordinate]*Cube)

	return u
}

// Cycle returns a univers as a result of applyed rules on the given one.
func (u *Universe) Cycle() *Universe {
	newUniverse := NewUniverse()

	for dz := u.minZ - 1; dz <= u.maxZ+1; dz++ {
		for dy := u.minY - 1; dy <= u.maxY+1; dy++ {
			for dx := u.minX - 1; dx <= u.maxX+1; dx++ {
				activeNeighbors := u.ActiveNeighbors(dx, dy, dz)
				numberOfactiveNeighbors := len(activeNeighbors)
				cube := u.GetCube(dx, dy, dz)

				if cube.IsActive() {
					// If a cube is active and exactly 2 or 3 of its neighbors are also active,
					// the cube remains active. Otherwise, the cube becomes inactive.
					if numberOfactiveNeighbors == 2 || numberOfactiveNeighbors == 3 {
						newUniverse.AddNewCube(dx, dy, dz).SetActive()
					}
				} else {
					// If a cube is inactive but exactly 3 of its neighbors are active,
					//  the cube becomes active. Otherwise, the cube remains inactive.
					if numberOfactiveNeighbors == 3 {
						newUniverse.AddNewCube(dx, dy, dz).SetActive()
					}
				}
			}
		}
	}

	return newUniverse
}

// CountActiveCubes returns the number of active cube in the universe
func (u *Universe) CountActiveCubes() int {
	var result int = 0

	for _, c := range u.universe {
		if c.IsActive() {
			result++
		}
	}

	return result
}

// ActiveNeighbors returns the coordinates of all active neighbors
func (u *Universe) ActiveNeighbors(x, y, z int) []UniverseCoordinate {
	var result = []UniverseCoordinate{}

	zRange := []int{z - 1, z, z + 1}
	yRange := []int{y - 1, y, y + 1}
	xRange := []int{x - 1, x, x + 1}

	for _, dz := range zRange {
		for _, dy := range yRange {
			for _, dx := range xRange {
				if dx == x && dy == y && dz == z {
					continue
				}
				if u.GetCube(dx, dy, dz).IsActive() {
					result = append(result, UniverseCoordinate{dx, dy, dz})
				}
			}
		}
	}

	// fmt.Printf("ActiveNeighbors(%d,%d,%d) = %v\n", x, y, z, result)
	return result
}

// AddNewCube adds a new inactive cube in the universe
func (u *Universe) AddNewCube(x, y, z int) *Cube {
	// Allways update universe dimension before adding Cube
	u.updateDimensions(x, y, z)

	// Create coordinate, cube and add it
	uc := UniverseCoordinate{x: x, y: y, z: z}
	c := NewCube()
	u.universe[uc] = c

	return c
}

// GetCube returns cube at given positions
func (u *Universe) GetCube(x, y, z int) *Cube {
	if cube, exists := u.universe[UniverseCoordinate{x, y, z}]; exists {
		return cube
	}

	return NewCube()
}

func (u *Universe) updateDimensions(x, y, z int) {
	// fmt.Printf("updateDimensions(%d,%d,%d)\n", x, y, z)

	if len(u.universe) == 0 {
		u.minX, u.maxX = x, x
		u.minY, u.maxY = y, y
		u.minZ, u.maxZ = z, z

		return
	}

	if x < u.minX {
		u.minX = x
	} else if x > u.maxX {
		u.maxX = x
	}

	if y < u.minY {
		u.minY = y
	} else if y > u.maxY {
		u.maxY = y
	}

	if z < u.minZ {
		u.minZ = z
	} else if z > u.maxZ {
		u.maxZ = z
	}

}

// String returns an ascii representation of universe
func (u *Universe) String() string {
	var result string

	for dz := u.minZ; dz <= u.maxZ; dz++ {
		result += fmt.Sprintf("z = %d\n", dz)

		for dy := u.minY; dy <= u.maxY; dy++ {

			for dx := u.minX; dx <= u.maxX; dx++ {

				if u.GetCube(dx, dy, dz).IsActive() {
					result += "#"
				} else {
					result += "."
				}

			}
			result += "\n"

		}
		result += "\n"

	}

	return result
}
